package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ServerHostnameResponse extends AbstractResponse {

    @NotNull
    private String hostname;

}
