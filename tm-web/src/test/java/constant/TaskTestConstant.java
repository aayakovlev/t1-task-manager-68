package constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

public final class TaskTestConstant {

    @NotNull
    public static TaskDTO TASK_ONE = new TaskDTO("First");

    @NotNull
    public static TaskDTO TASK_TWO = new TaskDTO("Second");

    @NotNull
    public static TaskDTO TASK_THREE = new TaskDTO("Third");

    @NotNull
    public static TaskDTO TASK_FOUR = new TaskDTO("Fourth");

}
