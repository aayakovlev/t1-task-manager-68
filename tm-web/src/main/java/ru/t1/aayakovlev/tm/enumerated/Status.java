package ru.t1.aayakovlev.tm.enumerated;

import java.util.Arrays;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        return Arrays.stream(values())
                .filter((s) -> value.equals(s.name()))
                .findFirst()
                .orElse(null);
    }

    public String getDisplayName() {
        return displayName;
    }

}
