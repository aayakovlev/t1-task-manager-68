package ru.t1.aayakovlev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;


public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/projects/";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/count")
    long count();

    @DeleteMapping("/delete")
    void deleteAll();

    @DeleteMapping("/delete/{id}")
    void deleteById(@PathVariable("id") @NotNull final String id);

    @GetMapping("/exists/{id}")
    boolean existsById(@PathVariable("id") @NotNull final String id);

    @NotNull
    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @NotNull
    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") @NotNull final String id);

    @NotNull
    @PutMapping("/add")
    ProjectDTO save(@NotNull final ProjectDTO project);

    @NotNull
    @PostMapping("/save/{id}")
    ProjectDTO update(@RequestBody @NotNull final ProjectDTO project);

}
