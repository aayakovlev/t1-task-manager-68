package ru.t1.aayakovlev.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.repository.ProjectDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectDTOService implements IProjectDTOService {

    @Getter
    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @Override
    public long count() throws AbstractException {
        return getRepository().count();
    }

    @Override
    @Transactional
    public void deleteAll() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new ProjectNotFoundException();
        getRepository().deleteById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsById(id);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() throws AbstractException {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public ProjectDTO findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> resultEntity = getRepository().findById(id);
        if (!resultEntity.isPresent()) throw new ProjectNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    public ProjectDTO save(@Nullable final ProjectDTO project) throws EntityEmptyException {
        if (project == null) throw new EntityEmptyException();
        return getRepository().save(project);
    }

}
