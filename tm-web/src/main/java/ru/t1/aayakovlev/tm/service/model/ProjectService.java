package ru.t1.aayakovlev.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.model.IProjectService;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public long count() throws AbstractException {
        return getRepository().count();
    }

    @Override
    @Transactional
    public void deleteAll() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new ProjectNotFoundException();
        getRepository().deleteById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsById(id);
    }

    @NotNull
    @Override
    public List<Project> findAll() throws AbstractException {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public Project findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Optional<Project> resultEntity = getRepository().findById(id);
        if (!resultEntity.isPresent()) throw new ProjectNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    public Project save(@Nullable final Project project) throws EntityEmptyException {
        if (project == null) throw new EntityEmptyException();
        return getRepository().save(project);
    }

}
