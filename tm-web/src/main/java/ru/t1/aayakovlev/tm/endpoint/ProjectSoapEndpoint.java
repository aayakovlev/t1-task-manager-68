package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aayakovlev.tm.api.endpoint.IProjectSoapEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

@Endpoint
public final class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://aayakovlev.t1.ru/tm/dto/soap";

    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(
            @RequestPayload @NotNull final ProjectCountRequest request
    ) throws AbstractException {
        return new ProjectCountResponse(service.count());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(
            @RequestPayload @NotNull final ProjectDeleteAllRequest request
    ) throws AbstractException {
        service.deleteAll();
        return new ProjectDeleteAllResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final ProjectDeleteByIdRequest request
    ) throws AbstractException {
        service.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(
            @RequestPayload @NotNull final ProjectExistsByIdRequest request
    ) throws AbstractException {
        return new ProjectExistsByIdResponse(service.existsById(request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(
            @RequestPayload @NotNull final ProjectFindAllRequest request
    ) throws AbstractException {
        return new ProjectFindAllResponse(service.findAll());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(
            @RequestPayload @NotNull final ProjectFindByIdRequest request
    ) throws AbstractException {
        return new ProjectFindByIdResponse(service.findById(request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(
            @RequestPayload @NotNull final ProjectSaveRequest request
    ) throws EntityEmptyException {
        return new ProjectSaveResponse(service.save(request.getProject()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    public ProjectUpdateResponse update(
            @RequestPayload @NotNull final ProjectUpdateRequest request
    ) throws EntityEmptyException {
        return new ProjectUpdateResponse(service.save(request.getProject()));
    }

}
