package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aayakovlev.tm.api.endpoint.ITaskSoapEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

@Endpoint
public final class TaskSoapEndpoint implements ITaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://aayakovlev.t1.ru/tm/dto/soap";

    @Autowired
    private ITaskDTOService service;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(
            @RequestPayload @NotNull final TaskCountRequest request
    ) throws AbstractException {
        return new TaskCountResponse(service.count());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(
            @RequestPayload @NotNull final TaskDeleteAllRequest request
    ) throws AbstractException {
        service.deleteAll();
        return new TaskDeleteAllResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final TaskDeleteByIdRequest request
    ) throws AbstractException {
        service.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(
            @RequestPayload @NotNull final TaskExistsByIdRequest request
    ) throws AbstractException {
        return new TaskExistsByIdResponse(service.existsById(request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(
            @RequestPayload @NotNull final TaskFindAllRequest request
    ) throws AbstractException {
        return new TaskFindAllResponse(service.findAll());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = NAMESPACE)
    public TaskFindAllByProjectIdResponse findAll(
            @RequestPayload @NotNull final TaskFindAllByProjectIdRequest request
    ) throws AbstractException {
        return new TaskFindAllByProjectIdResponse(service.findAllByProjectId(request.getProjectId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(
            @RequestPayload @NotNull final TaskFindByIdRequest request
    ) throws AbstractException {
        return new TaskFindByIdResponse(service.findById(request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(
            @RequestPayload @NotNull final TaskSaveRequest request
    ) throws EntityEmptyException {
        return new TaskSaveResponse(service.save(request.getTask()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse update(
            @RequestPayload @NotNull final TaskUpdateRequest request
    ) throws EntityEmptyException {
        return new TaskUpdateResponse(service.save(request.getTask()));
    }

}
