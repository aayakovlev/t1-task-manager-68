package ru.t1.aayakovlev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

public interface IProjectDTOService {

    long count() throws AbstractException;

    @Transactional
    void deleteAll() throws AbstractException;

    @Transactional
    void deleteById(@Nullable final String id) throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<ProjectDTO> findAll() throws AbstractException;

    @NotNull
    ProjectDTO findById(@Nullable final String id) throws AbstractException;

    @NotNull
    @Transactional
    ProjectDTO save(@Nullable final ProjectDTO project) throws EntityEmptyException;

}
