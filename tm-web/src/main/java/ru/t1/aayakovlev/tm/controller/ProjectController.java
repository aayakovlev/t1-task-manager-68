package ru.t1.aayakovlev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;

@Controller
@RequestMapping("/projects")
public final class ProjectController {

    @Autowired
    private IProjectDTOService service;

    @GetMapping("")
    public ModelAndView index() throws AbstractException {
        return new ModelAndView("project-list", "projects", service.findAll());
    }

    @GetMapping("/create")
    public String create() throws EntityEmptyException {
        service.save(new ProjectDTO("new Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id) throws AbstractException {
        service.deleteById(id);
        return "redirect:/projects";
    }

    @PostMapping("/edit/{id}")
    public String edit(
            @ModelAttribute("project") final ProjectDTO project,
            final BindingResult result
    ) throws EntityEmptyException {
        service.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) throws AbstractException {
        final ProjectDTO project = service.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
