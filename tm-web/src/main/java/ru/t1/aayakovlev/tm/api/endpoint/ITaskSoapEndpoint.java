package ru.t1.aayakovlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.endpoint.TaskSoapEndpoint;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

public interface ITaskSoapEndpoint {

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskCountResponse count(
            @RequestPayload @NotNull final TaskCountRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskDeleteAllResponse deleteAll(
            @RequestPayload @NotNull final TaskDeleteAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final TaskDeleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskExistsByIdResponse existsById(
            @RequestPayload @NotNull final TaskExistsByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskFindAllResponse findAll(
            @RequestPayload @NotNull final TaskFindAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskFindAllByProjectIdResponse findAll(
            @RequestPayload @NotNull final TaskFindAllByProjectIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskFindByIdResponse findById(
            @RequestPayload @NotNull final TaskFindByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskSaveResponse save(
            @RequestPayload @NotNull final TaskSaveRequest request
    ) throws EntityEmptyException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskUpdateResponse update(
            @RequestPayload @NotNull final TaskUpdateRequest request
    ) throws EntityEmptyException;

}
