package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public final class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService service;

    @Override
    @GetMapping("/count")
    public long count() throws AbstractException {
        return service.count();
    }

    @Override
    @DeleteMapping("/delete")
    public void deleteAll() throws AbstractException {
        service.deleteAll();
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws AbstractException {
        return service.findAll();
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @PutMapping("/add")
    public ProjectDTO save(
            @RequestBody @NotNull final ProjectDTO project
    ) throws EntityEmptyException {
        return service.save(project);
    }

    @NotNull
    @Override
    @PostMapping("/save/{id}")
    public ProjectDTO update(
            @RequestBody @NotNull final ProjectDTO project
    ) throws EntityEmptyException {
        return service.save(project);
    }

}
