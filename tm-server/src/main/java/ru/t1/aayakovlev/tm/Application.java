package ru.t1.aayakovlev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.aayakovlev.tm.component.ServerBootstrap;
import ru.t1.aayakovlev.tm.config.ServerConfig;

public final class Application {

    public static void main(@NotNull String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfig.class);
        @NotNull final ServerBootstrap bootstrap = context.getBean(ServerBootstrap.class);
        bootstrap.start();
    }

}
