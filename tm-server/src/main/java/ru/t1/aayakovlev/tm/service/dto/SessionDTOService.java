package ru.t1.aayakovlev.tm.service.dto;

import ru.t1.aayakovlev.tm.dto.model.SessionDTO;

public interface SessionDTOService extends BaseDTOService<SessionDTO> {

}
