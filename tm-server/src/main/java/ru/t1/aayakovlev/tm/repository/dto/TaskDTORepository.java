package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends ExtendedDTORepository<TaskDTO> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserIdAndProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

}
