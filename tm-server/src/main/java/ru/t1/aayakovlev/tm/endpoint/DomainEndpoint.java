package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface DomainEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "DomainEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static DomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static DomainEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, DomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static DomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, DomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse backupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBackupSaveResponse backupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBase64LoadResponse base64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBase64SaveResponse base64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBinaryLoadResponse binaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBinarySaveResponse binarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlResponse jsonLoadFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonLoadJaxBResponse jsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlResponse jsonSaveFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonSaveJaxBResponse jsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlLoadFasterXmlResponse xmlLoadFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlLoadJaxBResponse xmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlSaveFasterXmlResponse xmlSaveFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlSaveJaxBResponse xmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataYamlLoadFasterXmlResponse yamlLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlResponse yamlSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) throws AbstractException;

}
